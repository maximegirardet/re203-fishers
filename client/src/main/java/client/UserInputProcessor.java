package client;

import client.UserCommands.UserCommands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInputProcessor extends Thread {

	/**
	 * Main DisplayController instance
	 */
	private final DisplayController displayController;

	/**
	 * Standard input buffer
	 */
	private BufferedReader stdIn;

	public UserInputProcessor(DisplayController displayController) {
		this.displayController = displayController;
	}

	/**
	 * Tokenizes user input and instantiates correct class if user is allowed to send commands and if the command does exist.
	 *
	 * @param command User input
	 */
	private void processUserInput(String command) {
		String[] ArgumentArray = command.split("[ ,]+");
		if (isAllowedToSendCommand() && verifyIfCommandExists(ArgumentArray[0])) {
			assignUserCommand(ArgumentArray[0], ArgumentArray);

		} else {
			displayController.userOutput.displayError("The command you typed does not exist or you're not allowed to write commands for the moment.");
		}
	}

	/**
	 * Verifies if user is authorized to send commands
	 */
	private boolean isAllowedToSendCommand() {
		return displayController.socket.isConnected() && displayController.isGreeted() && !displayController.isWaitingForAnswer();
	}

	/**
	 * Verifies if the specified command exists
	 *
	 * @param firstWord The first word of user input
	 * @return A boolean which indicates if the command does exist.
	 */
	private boolean verifyIfCommandExists(String firstWord) {
		return UserCommands.commands.containsKey(firstWord);
	}

	/**
	 * Gets the UserCommands instance corresponding to the user input and begins sending commands if possible, or display usage if not.
	 *
	 * @param firstWord     Command
	 * @param ArgumentArray StringTokenizer of user input
	 */
	private void assignUserCommand(String firstWord, String[] ArgumentArray) {
		UserCommands instance = UserCommands.commands.get(firstWord);
		displayController.currentCommandInstance = instance;
		if (instance.verifySyntax(ArgumentArray)) {
			sendCommands(instance.translate(ArgumentArray));
			if (instance.shouldWaitForAnswer()) {
				displayController.setWaitingForAnswer(true);
			} else {
				displayController.userOutput.displaySuccess(displayController.currentCommandInstance.OKResponse());
			}
		} else {
			displayController.userOutput.displayError(instance.usage());
		}
	}

	/**
	 * Sends commands to server
	 *
	 * @param commands An array of commands to send
	 */
	private void sendCommands(String[] commands) {
		for (String command : commands) {
			displayController.socket.send(command);
		}
	}

	/**
	 * Starts a new thread which waits for user inputs.
	 */
	public void run() {
		stdIn = new BufferedReader(new InputStreamReader(System.in));
		try {
			waitForCommands();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Waits for user to send commands
	 */
	private void waitForCommands() throws IOException {
		String userInput;
		while ((userInput = stdIn.readLine()) != null) {
			processUserInput(userInput);
		}
	}
}
