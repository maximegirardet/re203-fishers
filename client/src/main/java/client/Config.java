package client;

import java.util.Properties;


public class Config {
	/**
	 * Property list with each key and its corresponding value in the config file
	 */
	final Properties configFile;

	/**
	 * Initialize an instance of Config
	 *
	 * @param path of the config file
	 */
	public Config(String path) {
		configFile = new java.util.Properties();
		try {
			configFile.load(this.getClass().getResourceAsStream(path));
		} catch (Exception eta) {
			eta.printStackTrace();
		}
	}

	/**
	 * Return the value corresponding to the key
	 *
	 * @param key corresponding to the configuration parameter we want
	 * @return A string which indicates the value of the key in the config file
	 */
	public String getProperty(Value key) {
		return this.configFile.getProperty(key.getName());
	}

	/**
	 * Enum of all config values.
	 */
	@SuppressWarnings("unused")
	public enum Value {
		ADDRESS("controller-address"),
		ID("id"),
		PORT("controller-port"),
		TIMEOUT("display-timeout-value"),
		RESOURCES("resources"),
		LOG_FILENAME("log-filename"),
		VERBOSITY("verbosity");

		/**
		 * Name of the parameter in the config file
		 */
		private final String name;

		Value(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}

	}

}

