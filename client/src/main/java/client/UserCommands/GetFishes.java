package client.UserCommands;

import client.InternalProtocol;

public class GetFishes implements UserCommands {

	@Override
	public boolean shouldWaitForAnswer() {
		return false;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 1;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		return new String[]{InternalProtocol.getFishes()};
	}

	@Override
	public String usage() {
		return "Usage : getFishes";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return true;
	}

	@Override
	public String NOKResponse() {
		return "";
	}

	@Override
	public String OKResponse() {
		return "";
	}

	@Override
	public void actionOnAnswer() {
	}
}
