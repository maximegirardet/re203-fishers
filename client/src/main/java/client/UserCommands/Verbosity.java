package client.UserCommands;

import client.Log;

import java.util.logging.Level;

public class Verbosity implements UserCommands {
	@Override
	public boolean shouldWaitForAnswer() {
		return false;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 2 && Log.deserializeLevel(ArgumentArray[1]) != null;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		Level level = Log.deserializeLevel(ArgumentArray[1]);
		Log.setLevel(level);
		Log.info("Verbosity set to " + level, true);
		return new String[0];
	}

	@Override
	public String usage() {
		StringBuilder usage = new StringBuilder("Usage : verbosity [");
		String[] logs = Log.getAcceptedVerbosityLevels();
		for (int i = 0; i < logs.length - 1; i++) {
			usage.append(logs[i]).append(" | ");
		}
		return usage.append(logs[logs.length - 1]).append("]").toString();
	}

	@Override
	public boolean verifyResponse(String Response) {
		return true;
	}

	@Override
	public String NOKResponse() {
		return "";
	}

	@Override
	public String OKResponse() {
		return "";
	}

	@Override
	public void actionOnAnswer() {
	}
}
