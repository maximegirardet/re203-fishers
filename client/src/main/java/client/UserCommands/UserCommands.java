package client.UserCommands;

import java.util.Map;

public interface UserCommands {

	/**
	 * Map of all userCommands instances.
	 */
	Map<String, UserCommands> commands = Map.ofEntries(
			Map.entry("status", new Status()),
			Map.entry("addFish", new AddFish()),
			Map.entry("startFish", new StartFish()),
			Map.entry("delFish", new DelFish()),
			Map.entry("verbosity", new Verbosity()),
			Map.entry("getFishes", new GetFishes())
	);

	/**
	 * Tells if the displayController should wait for an answer for this command.
	 *
	 * @return A boolean which indicates if we should wait for an answer
	 */
	boolean shouldWaitForAnswer();

	/**
	 * Verifies if the syntax of the command is correct
	 *
	 * @param ArgumentArray array of the command params
	 * @return A boolean which indicates if the command is valid
	 */
	boolean verifySyntax(String[] ArgumentArray);

	/**
	 * Translates the user command to an internal command
	 *
	 * @param ArgumentArray array of the command params
	 * @return The translated command
	 */
	String[] translate(String[] ArgumentArray);

	/**
	 * Gets the usage of the specified command
	 *
	 * @return A string representing usage
	 */
	String usage();

	/**
	 * Verifies if the response received match to the response expected for
	 * this command
	 *
	 * @param Response array of the response and its params
	 * @return A boolean which indicates if the command is valid
	 */
	boolean verifyResponse(String Response);

	/**
	 * Returns the message to send to the client when server respond NOK
	 *
	 * @return A string to print to the client
	 */
	String NOKResponse();

	/**
	 * Return the response for client if the response received from controller is OK
	 *
	 * @return A string which is the response for the client
	 */
	String OKResponse();

	/**
	 * Apply the necessary actions according to the answer
	 */
	void actionOnAnswer();
}
