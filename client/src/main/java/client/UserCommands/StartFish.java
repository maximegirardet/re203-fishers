package client.UserCommands;

import client.Fishes.FishList;
import client.InternalProtocol;


public class StartFish implements UserCommands {
	/**
	 * Fish that is started by the command
	 */
	private String startedFishName;

	@Override
	public boolean shouldWaitForAnswer() {
		return true;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 2;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		startedFishName = ArgumentArray[1];
		return new String[]{InternalProtocol.startFish(ArgumentArray[1])};
	}

	@Override
	public String usage() {
		return "Usage : startFish [FishName] \n Example : startFish PoissonRouge";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return Response.equals("OK") || Response.equals("NOK");
	}

	@Override
	public String NOKResponse() {
		return "NOK";
	}

	@Override
	public String OKResponse() {
		return "OK";
	}

	@Override
	public void actionOnAnswer() {
		FishList.fishList.get(startedFishName).setStartedStatus(client.Fishes.startedStatus.STARTED);
	}
}
