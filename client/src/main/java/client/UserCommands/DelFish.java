package client.UserCommands;

import client.InternalProtocol;


public class DelFish implements UserCommands {
	@Override
	public boolean shouldWaitForAnswer() {
		return true;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 2;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		return new String[]{InternalProtocol.delFish(ArgumentArray[1])};
	}

	@Override
	public String usage() {
		return "Usage : delFish [FishName] \n Example : delFish PoissonNain";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return Response.equals("OK") || Response.equals("NOK");
	}

	@Override
	public String NOKResponse() {
		return "NOK : non-existent fish ";
	}

	@Override
	public String OKResponse() {
		return "OK";
	}

	@Override
	public void actionOnAnswer() {
	}
}
