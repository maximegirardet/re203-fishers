package client;

import java.io.IOException;
import java.util.Map;
import java.util.logging.*;


public class Log {

	/**
	 * Root logger
	 */
	private static final Logger logger = Logger.getLogger("");

	/**
	 * Walker used to get caller class and method.
	 */
	private static final StackWalker walker = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);

	/**
	 * Map of the different logging levels
	 */
	private static final Map<String, Level> levelCorrespondence = Map.ofEntries(
			Map.entry("normal", Level.INFO),
			Map.entry("fine", Level.FINE),
			Map.entry("finer", Level.FINER),
			Map.entry("finest", Level.FINEST)
	);

	/**
	 * DisplayController instance
	 */
	private static DisplayController displayController;

	/**
	 * Gets the frame of the caller.
	 *
	 * @return The frame of the caller.
	 */
	private static StackWalker.StackFrame getCaller() {
		return walker.walk(stream1 -> stream1.skip(3)
				.findFirst()
				.orElse(null));
	}

	/**
	 * Adds a new log into the logging system
	 *
	 * @param level   The logging level
	 * @param message The message to log
	 */
	private static void log(Level level, String message) {
		StackWalker.StackFrame callerFrame = getCaller();
		logger.logp(level, callerFrame.getClassName(), callerFrame.getMethodName(), message);
	}

	/**
	 * Displays or not the message on user terminal
	 *
	 * @param message           The message
	 * @param displayOnTerminal A boolean which indicates if we should display the message on the terminal
	 */
	private static void processInfoOnTerminal(String message, boolean displayOnTerminal) {
		if (displayOnTerminal) {
			displayController.userOutput.displayInfo(message);
		}
	}

	/**
	 * Initializes the logging system by setting file name and verbosity
	 * Removes the default terminal logging
	 *
	 * @param displayController The displayController instance
	 */
	public static void initializeLogger(DisplayController displayController) {
		Log.displayController = displayController;
		Handler[] handlers = logger.getHandlers();
		for (Handler handler : handlers) {
			logger.removeHandler(handler);
		}
		try {
			FileHandler fileTxt = new FileHandler(displayController.getConfigValues().getProperty(Config.Value.LOG_FILENAME));
			SimpleFormatter formatterTxt = new SimpleFormatter();
			fileTxt.setFormatter(formatterTxt);
			logger.addHandler(fileTxt);
			setLevel(deserializeLevel(displayController.getConfigValues().getProperty(Config.Value.VERBOSITY)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Deserializes logging level into Level object.
	 * Returns null if the level is invalid.
	 *
	 * @param level A serialized level candidate
	 * @return The corresponding Level object, or null.
	 */
	public static Level deserializeLevel(String level) {
		if (!levelCorrespondence.containsKey(level)) {
			Log.warning("Unknown verbose mode given");
			return null;
		}
		return levelCorrespondence.get(level);
	}

	/**
	 * Gets a String array of all accepted logging levels.
	 *
	 * @return A String array of all accepted logging levels.
	 */
	public static String[] getAcceptedVerbosityLevels() {
		return levelCorrespondence.keySet().toArray(new String[0]);
	}

	/**
	 * Sets a new level for the logging system.
	 *
	 * @param level The level.
	 */
	public static void setLevel(Level level) {
		if (level != null) {
			logger.setLevel(level);
		}
	}

	/**
	 * Publishes an info-level log.
	 *
	 * @param message           The message
	 * @param displayOnTerminal A boolean which indicates if we should display the message on the terminal.
	 */
	public static void info(String message, boolean displayOnTerminal) {
		log(Level.INFO, message);
		processInfoOnTerminal(message, displayOnTerminal);
	}

	/**
	 * Publishes an fine-level log.
	 *
	 * @param message           The message
	 * @param displayOnTerminal A boolean which indicates if we should display the message on the terminal.
	 */
	@SuppressWarnings("unused")
	public static void fine(String message, boolean displayOnTerminal) {
		log(Level.FINE, message);
		processInfoOnTerminal(message, displayOnTerminal);
	}

	/**
	 * Publishes an finer-level log.
	 *
	 * @param message           The message
	 * @param displayOnTerminal A boolean which indicates if we should display the message on the terminal.
	 */
	@SuppressWarnings("unused")
	public static void finer(String message, boolean displayOnTerminal) {
		log(Level.FINER, message);
		processInfoOnTerminal(message, displayOnTerminal);
	}

	/**
	 * Publishes an finest-level log.
	 *
	 * @param message           The message
	 * @param displayOnTerminal A boolean which indicates if we should display the message on the terminal.
	 */
	@SuppressWarnings("unused")
	public static void finest(String message, boolean displayOnTerminal) {
		log(Level.FINEST, message);
		processInfoOnTerminal(message, displayOnTerminal);
	}

	/**
	 * Publishes a warning-level log.
	 *
	 * @param message The message
	 */
	public static void warning(String message) {
		log(Level.WARNING, message);
		displayController.userOutput.displayWarning(message);
	}

	/**
	 * Publishes an error-level log.
	 *
	 * @param message The message
	 */
	public static void error(String message) {
		log(Level.SEVERE, message);
		displayController.userOutput.displayError(message);
	}
}
