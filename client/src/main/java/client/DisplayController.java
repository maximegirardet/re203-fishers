package client;

import client.UserCommands.UserCommands;

import java.io.IOException;

public class DisplayController {

	/**
	 * Path to the config file
	 */
	private static final String configPath = "affichage.cfg";

	/**
	 * Processes server messages
	 */
	final ServerResponseProcessor serverResponseProcessor;

	/**
	 * Utility class used to output in terminal
	 */
	final UserOutput userOutput;

	/**
	 * Config class used to access the value of the config file
	 */
	private final Config configValues;

	/**
	 * Socket instance used to communicate with server
	 */
	ClientSocket socket;

	/**
	 * Current instance of client.UserCommands (from the last command)
	 */
	UserCommands currentCommandInstance;

	private boolean isWaitingForAnswer = false;

	private boolean isGreeted = false;

	public DisplayController() {
		configValues = new Config(configPath);
		userOutput = new UserOutput();
		Log.initializeLogger(this);
		makeConnection();
		UserInputProcessor userInputProcessor = new UserInputProcessor(this);
		userInputProcessor.start();
		serverResponseProcessor = new ServerResponseProcessor(this);
	}

	/**
	 * Starts socket connection
	 */
	private void makeConnection() {
		String hostname = "localhost";
		int port = Integer.parseInt(configValues.getProperty(Config.Value.PORT));
		try {
			socket = new ClientSocket(this, hostname, port, configValues.getProperty(Config.Value.ID), Integer.parseInt(configValues.getProperty(Config.Value.TIMEOUT)));
			socket.start();
		} catch (IOException e) {
			Log.error("No server found at " + hostname + ":" + port);
			System.exit(0);
		}
	}

	public boolean isGreeted() {
		return isGreeted;
	}

	public void setGreeted(boolean greeted) {
		isGreeted = greeted;
	}

	public boolean isWaitingForAnswer() {
		return isWaitingForAnswer;
	}

	public void setWaitingForAnswer(boolean waitingForAnswer) {
		isWaitingForAnswer = waitingForAnswer;
	}

	public Config getConfigValues() {
		return configValues;
	}
}
