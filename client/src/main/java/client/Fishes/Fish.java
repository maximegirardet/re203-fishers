package client.Fishes;

import client.FxController;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Status of the transition used to block new transitions while a transition is running
 */
enum transitionStatus {
	RUNNING, STOPPED
}

public class Fish {
	/**
	 * Default image to use when there is no corresponding image
	 * TODO : Read this from config file
	 */
	private static final String defaultImagePath = "PoissonRouge.png";

	/**
	 * Fish name
	 */
	private final String name;

	/**
	 * Fish illustration
	 */
	private final Image image;

	/**
	 * Lock object used to block new transitions when a transition is running
	 */
	private final Object Lock = new Object();

	/**
	 * Queue (FIFO) of transitions which are about to be applied
	 * Cannot be final because of synchronisation.
	 */
	@SuppressWarnings("CanBeFinal")
	BlockingQueue<FishTransitionSpecs> transitions = new LinkedBlockingQueue<>();

	/**
	 * Current specs of the fish (geometry and duration of the last applied transition)
	 */
	private FishTransitionSpecs currentSpecs;

	/**
	 * ImageView node associated to the fish
	 */
	private ImageView imageView;

	/**
	 * Status of transitions on the fish
	 * Variable is volatile so that is it not read from the cache of the CPU.
	 */
	private volatile transitionStatus transitionStatus = client.Fishes.transitionStatus.RUNNING;

	/**
	 * Status of the fish
	 */
	private startedStatus startedStatus = client.Fishes.startedStatus.NOT_STARTED;

	public Fish(String name, FishTransitionSpecs initialSpecs) {
		this.name = name;
		this.currentSpecs = initialSpecs;
		this.image = this.getImage(this.getImagePath(name));
	}

	/**
	 * Builds a transition specs from the command tokens
	 *
	 * @param commandTokens The tokens of the received command (example: "PoissonRouge at 80x10,30x30,3" or "PoissonRouge at 80x10,30x30")
	 * @return Transitions specs or null if the command is incorrect
	 */
	static private FishTransitionSpecs buildTransitionSpecs(String[] commandTokens) {
		FishGeometry fishGeometry = buildGeometry(new String[]{commandTokens[2], commandTokens[3]});
		if (fishGeometry != null) {
			int delay = 0;
			if (commandTokens.length == 5) {
				delay = Integer.parseInt(commandTokens[4]);
			}
			return new FishTransitionSpecs(fishGeometry, delay);
		}
		return null;
	}

	/**
	 * Creates a fish from command tokens
	 *
	 * @param commandTokens The tokens of the received command (example: "PoissonRouge at 80x10,30x30,3" or "PoissonRouge at 80x10,30x30")
	 * @return A fish or null if the command is incorrect
	 */
	static public Fish createFish(String[] commandTokens) {
		FishTransitionSpecs specs = buildTransitionSpecs(commandTokens);
		if (specs != null) {
			return new Fish(commandTokens[0], specs);
		}
		return null;
	}

	/**
	 * Builds the final geometry of the transition
	 *
	 * @param geometryCommand The command describing position and size (example : ["80x10","30x30"])
	 * @return A FishGeometry instance corresponding to the final geometry of the fish after the transition
	 */
	static private FishGeometry buildGeometry(String[] geometryCommand) {
		if (geometryCommand.length == 2) {
			Coordinates position = Coordinates.deserializeCoordinates(geometryCommand[0]);
			Coordinates size = Coordinates.deserializeCoordinates(geometryCommand[1]);
			if (position != null && size != null) {
				return new FishGeometry(position, size);
			}
		}
		return null;
	}

	/**
	 * Setter of the attribute statusStarted
	 */
	public void setStartedStatus(client.Fishes.startedStatus startedStatus) {
		this.startedStatus = startedStatus;
	}

	/**
	 * Creates initial ImageView for the fish
	 *
	 * @return The initial ImageView
	 */
	private ImageView createImageView() {
		ImageView imageView = new ImageView();
		imageView.setImage(image);
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(currentSpecs.finalGeometry.getSizeX());
		imageView.setFitHeight(currentSpecs.finalGeometry.getSizeY());
		imageView.setX(currentSpecs.finalGeometry.getX());
		imageView.setY(currentSpecs.finalGeometry.getY());
		imageView.setId(name);
		return imageView;
	}

	/**
	 * Gets the path of the illustration image of the fish
	 *
	 * @param fishName The name of the fish
	 * @return The path of the image
	 */
	private String getImagePath(String fishName) {
		String[] tokens = fishName.split("_");
		return tokens[0] + ".png";
	}

	/**
	 * Gets the illustration image of the fish from resources
	 *
	 * @param path The path of the image
	 * @return The illustration image of the fish
	 */
	private Image getImage(String path) {
		URL resource;
		if ((resource = getClass().getResource(path)) == null) {
			resource = getClass().getResource(Fish.defaultImagePath);
		}
		return new Image(String.valueOf(resource));
	}

	/**
	 * Displays the fish on the anchorPane for the first time and starts consuming the queue.
	 * TODO: Decide if we wait for the first apparition (delay)
	 */
	public void firstDisplay() {
		imageView = createImageView();
		FxController.getAnchorPane().getChildren().add(imageView);
		synchronized (Lock) {
			Lock.notifyAll();
			transitionStatus = client.Fishes.transitionStatus.STOPPED;
		}
		consumeTransitions();
	}

	/**
	 * Creates the transition corresponding to the specs et plays it.
	 * Prevent other transitions to be played for the moment.
	 *
	 * @param specs The specifications of the transition
	 */
	private void createTransition(FishTransitionSpecs specs) {
		TranslateTransition transition = new TranslateTransition(Duration.seconds(specs.duration), imageView);
		transition.setByY(specs.finalGeometry.getY() - currentSpecs.finalGeometry.getY());
		transition.setByX(specs.finalGeometry.getX() - currentSpecs.finalGeometry.getX());
		transition.setOnFinished(actionEvent -> onTransitionFinish(specs));
		synchronized (Lock) {
			Lock.notifyAll();
			transitionStatus = client.Fishes.transitionStatus.RUNNING;
		}
		Platform.runLater(transition::play);
	}

	/**
	 * Adapts fish size and position to current AnchorPane
	 */
	public void adaptToWindow() {
		currentSpecs.finalGeometry.scale();
		updatePosition(currentSpecs.finalGeometry);
	}

	/**
	 * Updates size and position of the fish with respect to the geometry fishGeometry
	 *
	 * @param fishGeometry The fishGeometry to adapt to
	 */
	private void updatePosition(FishGeometry fishGeometry) {
		imageView.setFitWidth(fishGeometry.getSizeX());
		imageView.setFitHeight(fishGeometry.getSizeY());
	}

	/**
	 * Finalizes fish state when a transition is finished
	 * Authorizes other transitions to be created and played.
	 *
	 * @param specs The specifications of the transition which just finished
	 */
	private void onTransitionFinish(FishTransitionSpecs specs) {
		updatePosition(specs.finalGeometry);
		currentSpecs = specs;
		synchronized (Lock) {
			Lock.notifyAll();
			transitionStatus = client.Fishes.transitionStatus.STOPPED;
		}
	}

	/**
	 * Updates fish when a command concerning this fish is received
	 * Adds the transition specifications to the queue.
	 * Acts like a producer.
	 *
	 * @param commandTokens The received command tokens
	 */
	public void updateFish(String[] commandTokens) {
		FishTransitionSpecs specs = buildTransitionSpecs(commandTokens);
		if (specs != null) {
			System.out.println("transition added");
			try {
				transitions.put(specs);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Starts a new thread which consumes the transition queue as soon as there is at least one element in it.
	 * Waits for transition status to be STOPPED to launch a new transition.
	 */
	private void consumeTransitions() {
		Thread t = new Thread(() -> {
			while (true) {
				try {
					FishTransitionSpecs specs = transitions.take();
					System.out.println("fish " + name);
					synchronized (Lock) {
						while (transitionStatus == client.Fishes.transitionStatus.RUNNING) {
							try {
								Lock.wait();
							} catch (InterruptedException e) {
								break;
							}
						}
					}
					createTransition(specs);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

	/**
	 * @return the name, position, size and startedStatus of the fish (ex : "Fish PoissonRouge at 56x89, 87x9, STARTED")
	 */
	@Override
	public String toString() {
		return "Fish " + name + " at " +
				currentSpecs.finalGeometry.getPercentX() + "x" + currentSpecs.finalGeometry.getPercentY() +
				", " + currentSpecs.finalGeometry.getPercentSizeX() + "x" + currentSpecs.finalGeometry.getPercentSizeY() +
				", " + startedStatus + "\n";
	}
}
