package client.Fishes;

import javafx.application.Platform;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FishList {

	/**
	 * Map of currently displayed fish (indexed by their name)
	 */
	public static final HashMap<String, Fish> fishList = new HashMap<>();

	/**
	 * A regexp used to split list command into fish specs
	 */
	private final static String regexp = "\\[(.*?)]";

	/**
	 * The compiled pattern used to split list command into fish specs
	 */
	private final static Pattern pattern = Pattern.compile(regexp);

	/**
	 * Splits the received list into transition spec.
	 * Calls process function for each transition spec.
	 *
	 * @param listCommand The received list command (example: "list [PoissonRouge at 40x40,30x30,5] [Dori at 50x80,30x30,5]").
	 */
	public static void splitList(String listCommand) {
		Matcher matcher = pattern.matcher(listCommand);
		while (matcher.find()) {
			processReceivedFish(matcher.group(1));
		}
	}

	/**
	 * Processes a received fish.
	 * Creates it on the pane if it does not exist.
	 * Updates it if it already exists.
	 *
	 * @param command The received command for this fish (example: "PoissonRouge at 40x40,30x30,5").
	 */
	private static void processReceivedFish(String command) {
		String[] commandTokens = command.split("[, ]");
		Fish fish;
		if (fishList.containsKey(commandTokens[0])) {
			fish = fishList.get(commandTokens[0]);
			fish.updateFish(commandTokens);
		} else {
			fish = Fish.createFish(commandTokens);
			if (fish != null) {
				fishList.put(commandTokens[0], fish);
				Platform.runLater(fish::firstDisplay);
			}
		}
	}
}
