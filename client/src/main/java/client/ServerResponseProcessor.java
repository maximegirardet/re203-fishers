package client;

import client.Fishes.FishList;

import java.util.Map;

interface ReceptionCommand {
	void execute(String[] tokens, String message);
}

public class ServerResponseProcessor {

	/**
	 * Function to execute when list command is received
	 */
	private final ReceptionCommand LIST = (tokens, message) -> FishList.splitList(message);

	/**
	 * Main display controller
	 */
	@SuppressWarnings("CanBeFinal")
	private DisplayController displayController;

	/**
	 * Function to execute when pong command is received
	 */
	private final ReceptionCommand PONG = (tokens, message) -> {
		if (tokens.length > 1 && displayController.getConfigValues().getProperty(Config.Value.ID).equals(tokens[1]))
			displayController.socket.setWaitingForPong(false);
	};

	/**
	 * Function to execute when greeting command is received
	 */
	private final ReceptionCommand GREETING = (tokens, message) -> {
		displayController.setGreeted(true);
		displayController.userOutput.displaySuccess("Display module is greeted !");
		displayController.socket.send(InternalProtocol.getFishesContinuously());
	};

	/**
	 * Function to execute when success command is received
	 */
	private final ReceptionCommand SUCCESS = (tokens, message) -> {
		displayController.currentCommandInstance.actionOnAnswer();
		displayController.userOutput.displaySuccess(displayController.currentCommandInstance.OKResponse());
	};

	/**
	 * Function to execute when failure command is received
	 */
	private final ReceptionCommand FAILURE = (tokens, message) -> displayController.userOutput.displayError(displayController.currentCommandInstance.NOKResponse());

	/**
	 * Function to execute when bye command is received
	 */
	private final ReceptionCommand BYE = (tokens, message) -> displayController.socket.propagateDisconnection();

	/**
	 * Correspondence map between received commands and functions to call
	 */
	private final Map<String, ReceptionCommand> CommandCorrespondence = Map.ofEntries(
			Map.entry("pong", PONG),
			Map.entry("greeting", GREETING),
			Map.entry("OK", SUCCESS),
			Map.entry("NOK", FAILURE),
			Map.entry("list", LIST),
			Map.entry("bye", BYE)
	);

	public ServerResponseProcessor(DisplayController displayController) {
		this.displayController = displayController;
	}

	/**
	 * Processes received message from server
	 *
	 * @param message received message
	 */
	public void onReception(String message) {
		message = message.trim();
		String[] tokens = message.split("[, ]");
		String firstWord = tokens[0];
		if (verifySyntax(firstWord)) {
			CommandCorrespondence.get(firstWord).execute(tokens, message);
			if (displayController.currentCommandInstance != null && displayController.currentCommandInstance.verifyResponse(message)) {
				displayController.setWaitingForAnswer(false);
			}
		}
	}

	/**
	 * Verifies syntax of the received command
	 *
	 * @param firstWord The command
	 * @return A boolean which indicates is the received command is valid
	 * TODO: Improve this function
	 */
	private boolean verifySyntax(String firstWord) {
		return CommandCorrespondence.containsKey(firstWord);
	}

}
