package client;

public class UserOutput {

	public static final String ANSI_RED = "\u001B[31m";

	public static final String ANSI_GREEN = "\u001B[32m";

	public static final String ANSI_YELLOW = "\u001B[33m";

	public static final String ANSI_BLUE = "\u001B[34m";

	public static final String ANSI_RESET = "\u001B[0m";

	private void display(String color, String message) {
		System.out.println(color + message + ANSI_RESET);
	}

	/**
	 * Display the specified message as a success message (green)
	 *
	 * @param message The message to display in stdout
	 */
	void displaySuccess(String message) {
		display(ANSI_GREEN, message);
	}

	/**
	 * Display the specified message as an error message (red)
	 *
	 * @param message The message to display in stdout
	 */
	void displayError(String message) {
		display(ANSI_RED, message);
	}

	/**
	 * Display the specified message as an warning message (yellow)
	 *
	 * @param message The message to display in stdout
	 */
	void displayWarning(String message) {
		display(ANSI_YELLOW, message);
	}

	/**
	 * Display the specified message as an info message (blue)
	 *
	 * @param message The message to display in stdout
	 */
	void displayInfo(String message) {
		display(ANSI_BLUE, message);
	}

	/**
	 * Locks user input
	 * TODO: Implement
	 */
	@SuppressWarnings({"unused", "EmptyMethod"})
	void lockInput() {

	}

	/**
	 * Unlocks user input
	 * TODO: Implement
	 */
	@SuppressWarnings({"unused", "EmptyMethod"})
	void unlockInput() {

	}

}
