package client;

import client.Fishes.FishList;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class FxController {

	private static final int tickDuration = 1;

	/**
	 * Main AnchorPane
	 */
	private static AnchorPane anchorPane;

	/**
	 * Sets primaryStage and initializes listeners for window resizing and maximizing
	 *
	 * @param primaryStage The primary stage
	 */
	public static void setPrimaryStage(Stage primaryStage) {
		FxController.anchorPane = (AnchorPane) primaryStage.getScene().lookup("#AnchorPane");
		adaptFishes(new ArrayList<>(List.of(
				primaryStage.heightProperty(),
				primaryStage.widthProperty())), primaryStage.maximizedProperty());
	}

	/**
	 * Adds EventListeners to specified properties.
	 *
	 * @param doubleProperties  Resizing properties (width and height)
	 * @param maximizedProperty Maximizing property
	 */
	private static void adaptFishes(ArrayList<ReadOnlyDoubleProperty> doubleProperties, ReadOnlyBooleanProperty maximizedProperty) {
		doubleProperties.forEach(property -> property.addListener((obs, oldVal, newVal) -> FishList.fishList.forEach((fishName, fish) -> fish.adaptToWindow())));
		maximizedProperty.addListener((obs, oldVal, newVal) -> new Thread(() -> {
			try {
				Thread.sleep(tickDuration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			FishList.fishList.forEach((fishName, fish) -> fish.adaptToWindow());
		}).start());
	}

	public static AnchorPane getAnchorPane() {
		return anchorPane;
	}

	public static int getTickDuration() {
		return tickDuration;
	}
}
