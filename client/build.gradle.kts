plugins {
    id("application")
    id("org.openjfx.javafxplugin") version "0.0.8"
}

repositories {
    mavenCentral()
}

dependencies {
}

javafx {
    version = "13"
    modules = listOf( "javafx.controls", "javafx.fxml" )
}

application {
    mainClass.set("client.Main")
}

tasks.getByName<JavaExec>("run") {
    standardInput = System.`in`
}