#ifndef FISHERS_REPL_COMMANDS_H
#define FISHERS_REPL_COMMANDS_H

#include "repl.h"

#include "../utils/regex_const.h"

/**
 * Add a view in the aquarium topology
 * add view N<id> <x>x<y>+<width>+<height>
 * alternative: add view N<id> <x>x<y>, <width>x<height>
 * @param args[0] view id
 * @param args[1], args[2] coordinates
 * @param args[3], args[4] dimensions
 * @example "add view N1 40x60+400+500"
 * @example "add view N2 100x200, 400x500"
 */
#define add_view_regex_prefix "add" OPT_SPACE "view" SPACE "N" NUMBER SPACE COORDS
#define add_view_regex        add_view_regex_prefix SEPARATOR COORDS END
#define add_view_regex_alt    add_view_regex_prefix "\\+" NUMBER "\\+" NUMBER END
#define add_view_arg_count 5

int repl__add_view(repl r, char **args);


/**
 * Delete a view in the aquarium topology
 * del view N<id>
 * @param args[0] the id of the view to be deleted
 * @example "del view N1"
 */
#define del_view_regex     "del" OPT_SPACE "view" SPACE "N" NUMBER END
#define del_view_arg_count 1

int repl__del_view(repl r, char **args);


/**
 * Show the aquarium topology
 * @example "show aquarium" or "show"
 */
#define show_aquarium_regex     "show" OPT_SPACE "(aquarium|$)" END
#define show_aquarium_arg_count 1

int repl__show_aquarium(repl r, char **args);


/**
 * Print the fishes in the aquarium
 * fishes
 */
#define fishes_regex     "fishes" END
#define fishes_arg_count 0

int repl__print_fishes(repl r, char **args);

/**
 * Update the aquarium by one tick
 * update
 */
#define update_regex      "(update|tick)" END
#define update_arg_count  1

int repl__update(repl r, char **args);

/**
 * Load an aquarium topology from file
 * load <filename>
 * @param args[0] a filename
 */
#define load_regex      "load" SPACE STRING
#define load_arg_count  1

int repl__load(repl r, char **args);

/**
 * Save an aquarium topology to file
 * save <filename>
 * @param args[0] a filename
 */
#define save_regex      "save" SPACE STRING
#define save_arg_count  1

int repl__save(repl r, char **args);

/** A fallback handler printing back the unknown command */
#define fallback_regex     "(.*)"
#define fallback_arg_count 1

int repl__fallback_handler(__attribute__((unused)) repl r, char **args);

#endif // FISHERS_REPL_COMMANDS_H
