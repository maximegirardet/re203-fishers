#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#include "../log/log.h"
#include "repl_internal.h"
#include "repl_commands.h"

/** The list of commands managed by the REPL */
struct command cmd[] = {
        {add_view_regex,      add_view_arg_count,      (command_handler) repl__add_view},
        {add_view_regex_alt,  add_view_arg_count,      (command_handler) repl__add_view},
        {del_view_regex,      del_view_arg_count,      (command_handler) repl__del_view},
        {show_aquarium_regex, show_aquarium_arg_count, (command_handler) repl__show_aquarium},
        {fishes_regex,        fishes_arg_count,        (command_handler) repl__print_fishes},
        {update_regex,        update_arg_count,        (command_handler) repl__update},
        {load_regex,          load_arg_count,          (command_handler) repl__load},
        {save_regex,          save_arg_count,          (command_handler) repl__save},
        {fallback_regex,      fallback_arg_count,      (command_handler) repl__fallback_handler},
};

repl repl__start() {
    log_info("Starting the REPL");
    repl r = calloc(1, sizeof(*r));
    r->parser = parser__init(cmd, sizeof(cmd) / sizeof(cmd[0]));
    r->running = true;

    pthread_create(&r->thread, NULL, (void *(*)(void *)) repl__thread, r);
    return r;
}

void repl__thread(repl r) {
    struct pollfd poll_fd = {.fd = STDIN_FILENO, .events = POLLIN};
    char buffer[BUFFER_SIZE];
    bool running = true;

    while (running) {
        int poll_res = poll(&poll_fd, 1, 500);

        if (poll_res == -1 || poll_fd.revents & (POLLNVAL | POLLERR | POLLHUP)) {
            log_error("Error while polling: %s", strerror(errno));
        }

        if (poll_fd.revents & (POLLIN)) {
            char *res = fgets(buffer, BUFFER_SIZE, stdin);
            if (!res) {
                // The REPL was closed, clean up
                repl__stop(r);
                return;
            } else
                parser__parse(r->parser, buffer, r);
        }

        pthread_mutex_lock(&r->mutex);
        running = r->running;
        pthread_mutex_unlock(&r->mutex);
    }
}

void repl__join(repl r) {
    pthread_join(r->thread, NULL);
}

void repl__stop(repl r) {
    log_info("Stopping the REPL");

    pthread_mutex_lock(&r->mutex);
    r->running = false;
    pthread_mutex_unlock(&r->mutex);

    repl__join(r);
}

void repl__free(repl r) {
    log_info("Freeing the REPL");
    parser__free(r->parser);
    free(r);
}