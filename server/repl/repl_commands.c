#include <stdio.h>

#include "../utils/geometry.h"
#include "../utils/string.h"
#include "../log/log.h"
#include "repl_internal.h"
#include "repl_commands.h"

int repl__add_view(__attribute__((unused)) repl r, char **args) {
    int view_id = (int) strtol(args[0], NULL, 10);
    int_pair pos = {strtoi(args[1]), strtoi(args[2])};
    int_pair dim = {strtoi(args[3]), strtoi(args[4])};

    return aq__add_view(aq, view_id, pos, dim);
}

int repl__del_view(__attribute__((unused)) repl r, char **args) {
    int view_id = strtoi(args[0]);
    return aq__del_view(aq, view_id);
}

int repl__show_aquarium(__attribute__((unused)) repl r, __attribute__((unused)) char **args) {
    aq__show(aq, stderr);
    return 0;
}

int repl__print_fishes(__attribute__((unused)) repl r, __attribute__((unused)) char **args) {
    aq__print_fishes(aq);
    return 0;
}

int repl__update(__attribute__((unused)) repl r, __attribute__((unused)) char **args) {
    aq__update(aq, 1);
    return 0;
}

int repl__load(__attribute__((unused)) repl r, char **args) {
    log_warn("[UNIMPLEMENTED] Should load the aquarium topology from %s", args[0]);
    return 1;
}

int repl__save(__attribute__((unused)) repl r, char **args) {
    log_warn("[UNIMPLEMENTED] Should save the aquarium topology from %s", args[0]);
    return 1;
}


int repl__fallback_handler(__attribute__((unused)) repl r, char **args) {
    log_error("Unknown or malformed command %s", args[0]);
    return 1;
}



