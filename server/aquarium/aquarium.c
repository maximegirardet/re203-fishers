#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "../log/log.h"
#include "aquarium.h"
#include "fish.h"
#include "view.h"

/**
 * Searches for the view view_id into the aquarium aq
 * @return the pointer to the view if found, NULL otherwise
 */
static view find_view(aquarium aq, int view_id) {
    pthread_mutex_lock(&aq->views_mutex);

    view viewp;
    STAILQ_FOREACH(viewp, &aq->views, next) {
        if (view_id == viewp->id) {
            pthread_mutex_unlock(&aq->views_mutex);
            return viewp;
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
    return NULL;
}

/**
 * Searches for a inactive view in the aquarium aq
 * @return the pointer to the view if found, NULL otherwise
 */
static view find_inactive_view(aquarium aq) {
    pthread_mutex_lock(&aq->views_mutex);

    view viewp;
    STAILQ_FOREACH(viewp, &aq->views, next) {
        if (!viewp->active) {
            pthread_mutex_unlock(&aq->views_mutex);
            return viewp;
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
    return NULL;
}

/**
 * Searches for the fish named name into the aquarium aq
 * @return the pointer to the fish if found, NULL otherwise
 */
static fish find_fish(aquarium aq, char *name) {
    pthread_mutex_lock(&aq->fishes_mutex);

    fish fishp;
    STAILQ_FOREACH(fishp, &aq->fishes, next) {
        if (strcmp(fishp->name, name) == 0) {
            pthread_mutex_unlock(&aq->fishes_mutex);
            return fishp;
        }
    }

    pthread_mutex_unlock(&aq->fishes_mutex);
    return NULL;
}

/**
 * Indicates if a number is a percentage
 * @return true if the condition above is satisfied, false otherwise
 */
static inline bool is_percentage(int value) {
    return (value >= 0 && value <= 100);
}

/**
 * Indicates if a fish's path (infinitesimal thus straight path) is in a view
 * @return true if condition above is satisfied, false otherwise
 */
static inline bool is_fish_path_in_view(view v, fish f) {
    return rectangle__is_intersection_non_empty((rectangle) {v->pos, int_pair__add(v->pos, v->dim)},
                                                (rectangle) {f->curr_pos, f->next_pos});
}

/** Normalizes the number target according to reference */
static inline int normalize(int target, int reference) {
    return round((double) target * 100 / reference);
}

/** Denormalizes the number target according to reference */
static inline int denormalize(int target, int reference) {
    return round((double) target * reference / 100);
}

aquarium aq__init() {
    log_info("Initializing an aquarium");
    aquarium aq = malloc(sizeof(*aq));

    // TODO: use a topology file to collect the width, height and views of an aquarium
    log_warn("[UNIMPLEMENTED] The topology is 2000x2000");
    aq->dim = (int_pair) {2000, 2000};

    STAILQ_INIT(&aq->fishes);
    STAILQ_INIT(&aq->views);

    pthread_mutex_init(&aq->fishes_mutex, NULL);
    pthread_mutex_init(&aq->views_mutex, NULL);

    return aq;
}

void aq__free(aquarium aq) {
    log_info("Freeing an aquarium");

    pthread_mutex_lock(&aq->fishes_mutex);
    fish fishp, fish_tmp;
    STAILQ_FOREACH_SAFE(fishp, &aq->fishes, next, fish_tmp) {
        STAILQ_REMOVE(&aq->fishes, fishp, fish, next);
        fish__free(fishp);
    }
    pthread_mutex_unlock(&aq->fishes_mutex);

    pthread_mutex_lock(&aq->views_mutex);
    view viewp, view_tmp;
    STAILQ_FOREACH_SAFE(viewp, &aq->views, next, view_tmp) {
        STAILQ_REMOVE(&aq->views, viewp, view, next);
        view__free(viewp);
    }
    pthread_mutex_unlock(&aq->views_mutex);

    free(aq);
}


bool aq__add_fish(aquarium aq, int view_id, char *name, int_pair pos, int_pair dim, char *behavior) {
    log_info("Adding a %s fish %s in view %d, at %dx%d, size: %dx%d",
             behavior, name, view_id, pos.x, pos.y, dim.x, dim.y);

    if (find_fish(aq, name)) {
        log_warn("Failed to add fish %s: name already in use", name);
        return false;
    }

    view v = find_view(aq, view_id);
    if (!v) {
        log_warn("Failed to add fish %s: view %d does not exist", name, view_id);
        return false;
    }

    if (!is_percentage(dim.x) || !is_percentage(dim.y) || !is_percentage(pos.x) || !is_percentage(pos.y)) {
        log_warn("Failed to add fish %s: one component of size or position is not valid", name);
        return false;
    }

    // Position of the fish is given regarding the view: thus it must be translated after being denormalized
    int_pair abs_pos = {v->pos.x + denormalize(pos.x, v->dim.x),
                        v->pos.y + denormalize(pos.y, v->dim.y)};
    int_pair abs_dim = {denormalize(dim.x, v->dim.x), denormalize(dim.y, v->dim.y)};

    fish f = fish__new(name, abs_pos, abs_dim, behavior);
    if (!f) {
        log_warn("Failed to add fish %s: unknown behavior", name);
        return false;
    }

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_INSERT_HEAD(&aq->fishes, f, next);
    pthread_mutex_unlock(&aq->fishes_mutex);

    return true;
}

bool aq__del_fish(aquarium aq, char *name) {
    log_info("Deleting fish %s", name);

    fish f = find_fish(aq, name);
    if (!f) {
        log_warn("Failed to delete fish %s: it does not exist", name);
        return false;
    }

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_REMOVE(&aq->fishes, f, fish, next);
    pthread_mutex_unlock(&aq->fishes_mutex);

    fish__free(f);
    return true;
}

bool aq__start_fish(aquarium aq, char *name) {
    log_info("Starting fish %s", name);

    fish f = find_fish(aq, name);
    if (!f) {
        log_warn("Failed to start fish %s: it does not exist", name);
        return false;
    }

    fish__start(f);
    return true;
}


bool aq__add_view(aquarium aq, int view_id, int_pair pos, int_pair dim) {
    log_info("Adding view %d at %dx%d, size: %dx%d", view_id, pos.x, pos.y, dim.x, dim.y);

    view v = find_view(aq, view_id);
    if (v) {
        log_warn("Failed to add view %d: id already in use", view_id);
        return false;
    }

    if (!rectangle__is_fitting_in((rectangle) {pos, int_pair__add(pos, dim)},
                                  (rectangle) {(int_pair) {0, 0}, aq->dim})) {
        log_warn("Failed to add view %d: one component of size or position is not valid", view_id);
        return false;
    }

    view nv = view__new(view_id, pos, dim);
    pthread_mutex_lock(&aq->views_mutex);
    STAILQ_INSERT_HEAD(&aq->views, nv, next);
    pthread_mutex_unlock(&aq->views_mutex);
    return true;
}

int aq__request_view(aquarium aq, int view_id) {
    view v = NULL;

    if (view_id)
        v = find_view(aq, view_id);

    if (!v || v->active) {
        v = find_inactive_view(aq);
        if (!v) {
            log_warn("Could not serve a view (%d was requested)", view_id);
            return -1;
        }
    }

    v->active = true;
    log_info("Serving view %d (%d was requested)", v->id, view_id);
    return v->id;
}

void aq__yield_view(aquarium aq, int view_id) {
    log_info("Yielding view %d", view_id);

    view v = find_view(aq, view_id);
    if (v)
        v->active = false;
}

bool aq__del_view(aquarium aq, int view_id) {
    log_info("Deleting view %d", view_id);

    view v = find_view(aq, view_id);
    if (!v) {
        log_warn("Failed to delete view %d: it does not exist", view_id);
        return false;
    }

    pthread_mutex_lock(&aq->views_mutex);
    STAILQ_REMOVE(&aq->views, v, view, next);
    pthread_mutex_unlock(&aq->views_mutex);

    view__free(v);
    return true;
}

/**
 * Extract fish information in a string
 * @note The coordinates are relative to the view
 */
string aq__get_fish_in_view(fish f, view v) {
    char *desc_template = " [%s at %ux%u,%ux%u,%u]";

    int x = normalize(f->next_pos.x - v->pos.x, v->dim.x);
    int y = normalize(f->next_pos.y - v->pos.y, v->dim.y);
    int width = normalize(f->dim.x, v->dim.x);
    int height = normalize(f->dim.y, v->dim.y);
    int time_interval = 5; // FIXME: Time interval should be variable

    return alloc_printf(desc_template, f->name, x, y, width, height, time_interval);
}

string aq__get_fishes_in_view(aquarium aq, int view_id) {
    view v = find_view(aq, view_id);
    if (!v) {
        log_error("Failed to get fishes in view %d: it does not exist", view_id);
        return NULL;
    }

    int list_length = 0;

    STAILQ_HEAD(desc_list, string) desc_list = STAILQ_HEAD_INITIALIZER(desc_list);
    fish fish;

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_FOREACH(fish, &aq->fishes, next) {
        if (is_fish_path_in_view(v, fish)) {
            string desc = aq__get_fish_in_view(fish, v);
            STAILQ_INSERT_TAIL(&desc_list, desc, next);
            list_length += desc->length;
        }
    }
    pthread_mutex_unlock(&aq->fishes_mutex);

    // Allocate a string to contain the fish descriptions and "list\n\0"
    int len = list_length + 4 + 1;
    char *list = calloc(len + 1, sizeof(char));
    string res = string__new(list, len);

    strcat(list, "list");
    char *list_ptr = list + 4;

    string desc, next_desc;
    STAILQ_FOREACH_SAFE(desc, &desc_list, next, next_desc) {
        strcat(list_ptr, desc->content);
        list_ptr += desc->length;
        string__free(desc);
    }

    *list_ptr = '\n';
    return res;
}

void aq__update(aquarium aq, int time_interval) {
    pthread_mutex_lock(&aq->fishes_mutex);

    fish fishp;
    STAILQ_FOREACH(fishp, &aq->fishes, next) {
        if (fishp->is_started)
            fish__move(fishp, aq->dim, time_interval);
    }

    pthread_mutex_unlock(&aq->fishes_mutex);
}

void aq__show(aquarium aq, FILE *stream) {
    fprintf(stream, "%dx%d\n", aq->dim.x, aq->dim.y);

    pthread_mutex_lock(&aq->views_mutex);

    if (STAILQ_EMPTY(&aq->views))
        fprintf(stream, "No views\n");
    else {
        view vp;
        STAILQ_FOREACH(vp, &aq->views, next) {
            view__show(vp, stream);
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
}

void aq__print_fishes(aquarium aq) {
    printf("Aquarium's fishes:\n");

    pthread_mutex_lock(&aq->fishes_mutex);

    fish fishp;
    int cpt = 0;
    STAILQ_FOREACH(fishp, &aq->fishes, next) {
        printf("> %d: %s\n", cpt, fishp->name);
        cpt++;
    }

    pthread_mutex_unlock(&aq->fishes_mutex);

    if (!cpt)
        printf("EMPTY\n");
    printf("\n");
}
