#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../log/log.h"
#include "fish.h"
#include "fish_behavior.h"


fish fish__new(const char *name, int_pair pos, int_pair dim, char *behavior) {
    if (!behavior || !fish_behavior__find(behavior)) {
        log_warn("Could not created fish %d: unknown behavior %s", name, behavior);
        return NULL;
    }
    fish f = malloc(sizeof(*f));

    f->name = calloc(sizeof(char), strlen(name) + 1);
    strcpy(f->name, name);
    f->dim = dim;
    f->curr_pos = pos;
    f->next_pos = pos;
    f->is_started = false;
    f->behavior = &fish_behavior__random;
    return f;
}

void fish__free(fish f) {
    free(f->name);
    free(f);
}

void fish__start(fish f) {
    f->is_started = true;
}

bool fish__move(fish f, int_pair aq_dim, int time_interval) {
    if (f->is_started) {
        f->curr_pos = f->next_pos;
        int_pair next_pos = f->behavior(f, aq_dim, time_interval);
        f->next_pos = next_pos;
        return true;
    }

    log_warn("Fish %s cannot move: it has not been started", f->name);
    return false;
}

const char *fish__print(fish f) {
    printf("name: %s\n"
           "is_started: %d\n"
           "width: %d\n"
           "height: %d\n"
           "current_x: %d\n"
           "current_y: %d\n"
           "next_x: %d\n"
           "next_y: %d\n\n",
           f->name, f->is_started, f->dim.x, f->dim.y, f->curr_pos.x, f->curr_pos.y, f->next_pos.x, f->next_pos.y);
    return f->name;
}
