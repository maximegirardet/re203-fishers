#include <string.h>

#include "fish_behavior.h"

#define MAX_SPEED 1 // unit(s) per second

const char *behavior_names[] = {"random", "RandomWayPoint", "horizontal", "HorizontalWayPoint", NULL};
const fish_behavior behavior_functions[] = {fish_behavior__random, fish_behavior__random, fish_behavior__horizontal,
                                             fish_behavior__horizontal};

fish_behavior fish_behavior__find(char *wanted_behavior) {
    int i = 0;
    while (behavior_names[i]) {
        if (strcmp(behavior_names[i], wanted_behavior) == 0)
            return behavior_functions[i];
        i++;
    }
    return NULL;
}

/**
 * Compute the allowed positions of the fish according to its time_interval and the dimension of the aquarium
 * @return a rectangle representing the allowed area
 */
static rectangle fish_allowed_positions(fish f, int_pair aq_dim, int time_interval) {
    int max_dist = MAX_SPEED * time_interval;
    rectangle fish_halo = int_pair__halo(f->curr_pos, max_dist);

    return rectangle__intersection(fish_halo, (rectangle) {(int_pair) {0, 0}, aq_dim});
}

int_pair fish_behavior__random(fish f, int_pair aq_dim, int time_interval) {
    rectangle allowed_positions = fish_allowed_positions(f, aq_dim, time_interval);
    int_pair next_pos = rectangle__random_point(allowed_positions);
    return next_pos;
}

int_pair fish_behavior__horizontal(fish f, int_pair aq_dim, int time_interval) {
    int max_dist = MAX_SPEED * time_interval;
    int direction = rand() % 2;
    int dx;
    if (direction == 0) // left
        dx = -rand() % MAX(f->curr_pos.x - max_dist, 0);
    else // right
        dx = rand() % MIN(f->curr_pos.x + max_dist, aq_dim.x);
    return (int_pair) {f->curr_pos.x + dx, 0};
}