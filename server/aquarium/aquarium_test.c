#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdlib.h>

#include "aquarium.h"

static aquarium a;

/**
 * A helper to instantiate a new aquarium before each test.
 */
static int aquarium_setup(__attribute__((__unused__)) void **state) {
    assert_ptr_equal(a, NULL);
    a = aq__init(); // FIXME: The default size is 2000x2000
    assert_ptr_not_equal(a, NULL);
    return 0;
}

/**
 * A helper to clean the aquairum after each test
 */
int aquarium_teardown(__attribute__((__unused__)) void **state) {
    assert_ptr_not_equal(a, NULL);
    aq__free(a);
    a = NULL;
    return 0;
}

/**
 * TEST: The aquarium should allow the creation of several views
 */
static void aquarium__add_view_test(__attribute__((__unused__)) void **state) {
    bool res;

    // Nominal situation
    res = aq__add_view(a, 1, (int_pair) {0, 0}, (int_pair) {1280, 720});
    assert_true(res);
    res = aq__add_view(a, 2, (int_pair) {1000, 400}, (int_pair) {200, 300});
    assert_true(res);

    // Unfitting view
    res = aq__add_view(a, 3, (int_pair) {1500, 1600}, (int_pair) {200, 600});
    assert_false(res);

    // Already using view_id
    res = aq__add_view(a, 2, (int_pair) {500, 500}, (int_pair) {200, 600});
    assert_false(res);
}

/**
 * TEST: The aquarium allows to retrieve the id of an existing view
 */
static void aquarium__request_view_test(__attribute__((__unused__)) void **state) {
    int res;
    aq__add_view(a, 1, (int_pair) {0, 0}, (int_pair) {1280, 720});
    aq__add_view(a, 3, (int_pair) {1000, 400}, (int_pair) {200, 300});

    // Nominal situation
    res = aq__request_view(a, 1);
    assert_int_equal(res, 1);

    res = aq__request_view(a, 3);
    assert_int_equal(res, 3);

    // A requested view is unavailable
    res = aq__request_view(a, 3);
    assert_int_equal(res, -1);

    aq__add_view(a, 2, (int_pair) {0, 0}, (int_pair) {1280, 720});

    // If possible, a request on a unavailable view returns an alternative
    res = aq__request_view(a, 1);
    assert_int_equal(res, 2);
}

/**
 * TEST: The aquarium allows the deletion of views
 */
static void aq__del_view_test(__attribute__((__unused__)) void **state) {
    bool res;
    aq__add_view(a, 4, (int_pair) {0, 0}, (int_pair) {1280, 720});

    // Can delete existing view
    res = aq__del_view(a, 4);
    assert_true(res);

    // Can't delete an already delete view
    res = aq__del_view(a, 4);
    assert_false(res);

    // Can't delete a non existing view
    res = aq__del_view(a, 12);
    assert_false(res);
}

/**
 * TEST: The aquarium allows the addition of fish
 * TODO: Use other strategies when available
 */
static void aq__add_fish_test(__attribute__((__unused__)) void **state) {
    bool res;
    aq__add_view(a, 1, (int_pair) {0, 0}, (int_pair) {100, 200});

    // Nominal situation
    res = aq__add_fish(a, 1, "Goldfish", (int_pair) {5, 10}, (int_pair) {20, 10}, "random");
    assert_true(res);

    res = aq__add_fish(a, 1, "Pufferfish", (int_pair) {20, 15}, (int_pair) {100, 100}, "random");
    assert_true(res);

    // Can't add a fish to a non-existing view
    res = aq__add_fish(a, 10, "Catfish", (int_pair) {10, 20}, (int_pair) {5, 10}, "random");
    assert_false(res);

    // Can't add a fish with unknown behavior
    res = aq__add_fish(a, 10, "Shark", (int_pair) {10, 20}, (int_pair) {50, 25}, "predator");
    assert_false(res);

    // Can't add a fish if the name is already in use
    res = aq__add_fish(a, 10, "Goldfish", (int_pair) {30, 20}, (int_pair) {10, 20}, "random");
    assert_false(res);
}

/**
 * TEST: The aquarium allows the deletion of fish
 */
static void aq__del_fish_test(__attribute__((__unused__)) void **state) {
    bool res;
    aq__add_view(a, 1, (int_pair) {0, 0}, (int_pair) {100, 200});
    aq__add_fish(a, 1, "Goldfish", (int_pair) {5, 10}, (int_pair) {20, 10}, "random");

    // Nominal situation
    res = aq__del_fish(a, "Goldfish");
    assert_true(res);

    // Can't delete the same fish twice
    res = aq__del_fish(a, "Goldfish");
    assert_false(res);

    // Can't delete non-existing fish
    res = aq__del_fish(a, "Sharky");
    assert_false(res);
}

/** TEST: The aquarium allows to retrive fish lists relative to the veiws */
static void aq__get_fishes_in_view_test(__attribute__((__unused__)) void **state) {
    aq__add_view(a, 1, (int_pair) {0, 0}, (int_pair) {2000, 2000});
    aq__add_view(a, 2, (int_pair) {0, 0}, (int_pair) {1000, 1000});

    aq__add_fish(a, 1, "Goldfish", (int_pair) {5, 10}, (int_pair) {20, 10}, "random");
    aq__add_fish(a, 2, "Butterflyfish", (int_pair) {13, 30}, (int_pair) {50, 25}, "random");

    string res = aq__get_fishes_in_view(a, 1);
    assert_string_equal(res->content, "list [Butterflyfish at 7x15,25x13,5] [Goldfish at 5x10,20x10,5]\n");
    string__free(res);

    res = aq__get_fishes_in_view(a, 2);
    assert_string_equal(res->content, "list [Butterflyfish at 13x30,50x25,5] [Goldfish at 10x20,40x20,5]\n");
    string__free(res);
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test_setup_teardown(aquarium__add_view_test, aquarium_setup, aquarium_teardown),
            cmocka_unit_test_setup_teardown(aquarium__request_view_test, aquarium_setup, aquarium_teardown),
            cmocka_unit_test_setup_teardown(aq__del_view_test, aquarium_setup, aquarium_teardown),
            cmocka_unit_test_setup_teardown(aq__add_fish_test, aquarium_setup, aquarium_teardown),
            cmocka_unit_test_setup_teardown(aq__del_fish_test, aquarium_setup, aquarium_teardown),
            cmocka_unit_test_setup_teardown(aq__get_fishes_in_view_test, aquarium_setup, aquarium_teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}