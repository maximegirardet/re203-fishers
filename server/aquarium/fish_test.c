#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "fish.h"

fish f;
static int_pair aq_dim = {100, 100};
static int time_interval = 5;

/** A helper to instantiate a fresh fish before each test */
int fish_setup(__attribute__((__unused__)) void **state) {
    assert_ptr_equal(f, NULL);

    int_pair pos = {0, 0};
    int_pair dim = {10, 10};
    f = fish__new("GoldFish", pos, dim, "random");

    return 0;
}

int horizontal_fish_setup(__attribute__((__unused__)) void **state) {
    assert_ptr_equal(f, NULL);

    int_pair pos = {0, 0};
    int_pair dim = {10, 10};
    f = fish__new("GoldFish", pos, dim, "horizontal");

    return 0;
}

/** A helper to clean the eat the fish after each test */
int fish_teardown(__attribute__((__unused__)) void **state) {
    assert_ptr_not_equal(f, NULL);
    fish__free(f);
    f = NULL;
    return 0;
}

/** TEST: Shouldn't be able to create a fish with an unknown behavior */
static void fish__rejects_unknown_behavior(__attribute__((__unused__)) void **state) {
    fish f1 = fish__new("WrongFish", (int_pair) {0, 0}, (int_pair) {10, 10}, "wrong_behavior");
    assert_ptr_equal(f1, NULL);
    fish f2 = fish__new("WrongFish", (int_pair) {0, 0}, (int_pair) {10, 10}, NULL);
    assert_ptr_equal(f2, NULL);
}

/** TEST: An unstarted fish shouldn't be able to move */
static void fish__cant_update_when_not_started(__attribute__((__unused__)) void **state) {
    assert_false(fish__move(f, aq_dim, time_interval));
    assert_true(int_pair__equal(f->curr_pos, (int_pair) {0, 0}));
    assert_true(int_pair__equal(f->next_pos, (int_pair) {0, 0}));
}

/** TEST: An started fish moves properly */
static void fish__update_when_started(__attribute__((__unused__)) void **state) {
    fish__start(f);
    fish__move(f, aq_dim, time_interval);

    for (int i = 0; i < 1000; i++) {
        // Check that the fish stays inside the aquarium
        int_pair pos1 = f->next_pos;
        assert_in_range(pos1.x, 0, aq_dim.x);
        assert_in_range(pos1.y, 0, aq_dim.y);

        fish__move(f, aq_dim, time_interval);

        // Assert that the fish position was updated
        int_pair pos2 = f->curr_pos;
        assert_true(pos2.x == pos1.x && pos2.y == pos1.y);
    }
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(fish__rejects_unknown_behavior),
            cmocka_unit_test_setup_teardown(fish__cant_update_when_not_started, fish_setup, fish_teardown),
            cmocka_unit_test_setup_teardown(fish__update_when_started, fish_setup, fish_teardown),
            cmocka_unit_test_setup_teardown(fish__update_when_started, horizontal_fish_setup, fish_teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}