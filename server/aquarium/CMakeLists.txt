add_library(fish fish.c fish.h fish_behavior.c fish_behavior.h)
target_link_libraries(fish geometry log)

add_library(view view.c view.h)
target_link_libraries(view geometry)

add_library(aquarium aquarium.c aquarium.h)
target_link_libraries(aquarium fish view string geometry m log)

add_executable(aquarium_test aquarium_test.c)
target_link_libraries(aquarium_test fish view aquarium cmocka)
add_test(NAME aquarium_test COMMAND aquarium_test)

add_executable(fish_test fish_test.c)
target_link_libraries(fish_test fish cmocka)
add_test(NAME fish_test COMMAND fish_test)


