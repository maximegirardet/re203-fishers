#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "geometry.h"

/** TEST: Overlapping rectangles should intersect correctly */
static void rectangle__correct_intersection(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {5, 5}, (int_pair) {15, 15}};
    rectangle r2 = {(int_pair) {10, 10}, (int_pair) {20, 20}};

    rectangle r3 = rectangle__intersection(r1, r2);
    assert_true(int_pair__equal(r3.tl, (int_pair) {10, 10}));
    assert_true(int_pair__equal(r3.br, (int_pair) {15, 15}));
}

/** TEST: Non-overlapping rectangles should intersect into a reversed rectangle */
static void rectangle__empty_intersection(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {5, 5}, (int_pair) {15, 15}};
    rectangle r2 = {(int_pair) {20, 20}, (int_pair) {30, 30}};

    rectangle r3 = rectangle__intersection(r1, r2);
    assert_true(int_pair__is_smaller(r3.br, r3.tl));
}

/** TEST: A random point should be in the rectangle */
static void rectangle__random_point_is_inside(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {0, 0}, (int_pair) {10, 10}};
    int_pair pos = rectangle__random_point(r1);
    assert_in_range(pos.x, 0, 10);
    assert_in_range(pos.y, 0, 10);

    rectangle r2 = {(int_pair) {10, 10}, (int_pair) {20, 20}};
    int_pair pos2 = rectangle__random_point(r2);
    assert_in_range(pos2.x, 10, 20);
    assert_in_range(pos2.y, 10, 20);
}

/** TEST: A halo should be correctly computed */
static void int_pair__computes_halo(__attribute__((__unused__)) void **state) {
    int_pair pos = {10, 10};
    rectangle r = int_pair__halo(pos, 5);

    assert_true(int_pair__equal(r.tl, (int_pair) {5,5}));
    assert_true(int_pair__equal(r.br, (int_pair) {15,15}));
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(rectangle__correct_intersection),
            cmocka_unit_test(rectangle__empty_intersection),
            cmocka_unit_test(rectangle__random_point_is_inside),
            cmocka_unit_test(int_pair__computes_halo),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}