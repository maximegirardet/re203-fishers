#ifndef FISHERS_GEOMETRY_H
#define FISHERS_GEOMETRY_H

#include <stdbool.h>
#include <stdlib.h>

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

/**
 * A structure to ease coordinates handling
 */
typedef struct int_pair {
    int x;
    int y;
} int_pair;

/**
 * A structure representing to rectangle using two points
 */
typedef struct rectangle {
    int_pair tl; /**< the top-left corner */
    int_pair br; /**< the bottom-right corner */
} rectangle;

/**
 * Compute the sum of two int_pairs
 */
static inline int_pair int_pair__add(int_pair a, int_pair b) {
    return (int_pair) {a.x + b.x, a.y + b.y};
}

static inline int_pair int_pair__diff(int_pair a, int_pair b) {
    return (int_pair) {a.x - b.x, a.y - b.y};
}

static inline int_pair int_pair_mod(int_pair a, int_pair b) {
    return (int_pair) {a.x % b.x, a.y % b.y};
}

static inline int_pair int_pair__random() {
    return (int_pair) {rand(), rand()};
}

static inline bool int_pair__equal(int_pair a, int_pair b) {
    return (a.x == b.x && a.y == b.y);
}

static inline bool int_pair__is_smaller(int_pair a, int_pair b) {
    return (a.x <= b.x && a.y <= b.y);
}

static inline bool int_pair__is_positive(int_pair p) {
    return (p.x >= 0 && p.y >= 0);
}

static inline bool int_pair__is_in_rectangle(int_pair p, rectangle r) {
    return (r.tl.x <= p.x && p.x <= r.br.x && r.tl.y <= p.y && p.y <= r.br.y);
}


/**
 * Is the rectangle r1 fitting the rectangle r2 ?
 */
static inline bool rectangle__is_fitting_in(rectangle r1, rectangle r2) {
    return (int_pair__is_in_rectangle(r1.tl, r2) && int_pair__is_in_rectangle(r1.br, r2));
}

/**
 * Is the intersection of two rectangles non empty ?
 */
static inline bool rectangle__is_intersection_non_empty(rectangle r1, rectangle r2) {
    return !(r1.br.y <= r2.tl.y || r1.tl.y >= r2.br.y || r1.br.x <= r2.tl.x || r1.tl.x >= r2.br.x);
}

/**
 * Compute the intersection of two rectangles
 * @note if the intersection is empty, br <= tl
 */
static inline rectangle rectangle__intersection(rectangle r1, rectangle r2) {
    return (rectangle) {(int_pair) {MAX(r1.tl.x, r2.tl.x), MAX(r1.tl.y, r2.tl.y)},
                        (int_pair) {MIN(r1.br.x, r2.br.x), MIN(r1.br.y, r2.br.y)}};
}

/**
 * Compute a random point in a rectangle
 */
static inline int_pair rectangle__random_point(rectangle r) {
    return int_pair__add(int_pair_mod(int_pair__random(), int_pair__diff(r.br, r.tl)), r.tl);
}

/**
 * Compute a squared halo of ray distance around a point
 * @return the squared halo
 */
static inline rectangle int_pair__halo(int_pair point, int ray) {
    return (rectangle) {int_pair__diff(point, (int_pair) {ray, ray}),
                        int_pair__add(point, (int_pair) {ray, ray})};
}

#endif // FISHERS_GEOMETRY_H
