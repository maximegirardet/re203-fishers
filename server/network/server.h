#ifndef FISHERS_SERVER_H
#define FISHERS_SERVER_H

#define SOCKET_QUEUE_LEN 10

/** An opaque structure representing a client */
typedef struct server *server;

/** Initialize a new server */
server server__init(int port);

/** Start the server and start accepting incoming connections */
void server__start(server s);

/** Disconnect the clients, stop the server and free the memory */
void server__stop(server s);

#endif // FISHERS_SERVER_H
