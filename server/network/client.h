#ifndef FISHERS_CLIENT_H
#define FISHERS_CLIENT_H

#include <netinet/ip.h>

#include "../utils/queue.h"
#include "client.h"

/** An opaque structure representing a client */
typedef struct client *client;

/** The internal representation of a client */
struct client {
    int socket; /**< The client socket file descriptor */
    pthread_t handler; /**< The thread executing the handler() function */
    bool connected; /** Is the client connected? */
    pthread_mutex_t mutex; /**< A mutex to protect `connected` */
    int view_id; /**< The view linked to the client */
    STAILQ_ENTRY(client) next; /**< The next client in the queue */
};

/** Initialize a new client */
client client__new(int socket);

/** Thread function to handle incoming messages */
void client__handler(client c);

/** Disconnect a client */
void client__disconnect(client c);

/** Shutdown a client and free the memory */
void client__join(client c);


#endif //FISHERS_CLIENT_H
