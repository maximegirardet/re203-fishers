#ifndef FISHERS_CLIENT_CMD_H
#define FISHERS_CLIENT_CMD_H

#include "../parser/parser.h"
#include "../aquarium/aquarium.h"
#include "../utils/regex_const.h"
#include "client.h"
#include "server.h"

extern parser client_parser;
extern aquarium aq;
extern server serv;

#define hello_success_answer "greeting N%d\n"
#define hello_fail_answer    "no greeting\n"

/**
 * Assign the next available view to the client
 * @return "greeting N1" or "NOK"
 */
#define hello_simple_regex     "hello"
#define hello_simple_arg_count 0

int client__hello_simple(client c, __attribute__((unused)) char **args);

/**
 * Associate the client to the requested view, or another one on error
 * "hello in as N<id>"
 * @param args[0] the requested view_id
 * @example "hello in as N1"
 * @return "greeting N1" or "NOK"
 */
#define hello_id_regex     "hello in as N" NUMBER END
#define hello_id_arg_count 1

int client__hello_id(client c, char **args);

/**
 * Disconnect the client
 * "log out"
 * @return "bye"
 */
#define log_out_regex     "log out" END
#define log_out_arg_count 0

int client__log_out(client c, char **args);

/**
 * Add a fish in the client view
 * "addFish <name> at <x>x<y>, <width>x<height>, <behavior>"
 * @param args[0] name
 * @param args[1], args[2] coordinates
 * @param args[3], args[4] dimensions
 * @param args[5] behavior
 * @example "addFish PoissonRouge at 90x40, 10x4, random"
 * @return "OK" or "NOK"
 */
#define add_fish_regex     \
        "addFish" SPACE STRING SPACE "at" SPACE COORDS SEPARATOR COORDS SEPARATOR STRING END
#define add_fish_arg_count 6

int client__add_fish(client c, char **args);


/**
 * Delete a fish from the aquarium
 * "delFish <name>"
 * @param args[0] fish name
 * @example "delFish PoissonRouge"
 * @return "OK" or "NOK"
 */
#define del_fish_regex     "delFish" SPACE STRING END
#define del_fish_arg_count 1

int client__del_fish(client c, char **args);


/**
 * Start a fish
 * "startFish <name>"
 * @param args[0] fish name
 * @example "startFish PoissonRouge"
 * @return "OK" or "NOK"
 */
#define start_fish_regex     "startFish" SPACE STRING END
#define start_fish_arg_count 1

int client__start_fish(client c, char **args);


/**
 * Announce that the client is still up
 * ping <message>
 * @return "pong <message>"
 */
#define ping_regex     "ping" OPT_SPACE STRING END
#define ping_arg_count 1

int client__ping(client c, char **args);

/**
 * Get the fishes in the client view
 * @return a list of fish
 */
#define get_fishes_regex "getFishes" END
#define get_fishes_count 0

int client__get_fishes(client c, char **args);

#endif // FISHERS_CLIENT_CMD_H
