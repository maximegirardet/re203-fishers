#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "parser.h"

/**
 * A global parser instance used to run the tests.
 */
parser p = NULL;


/**
 * A mock command_handler expecting none or one argument
 * @param args the arguments matched in the parsed command
 */
int mock_handler(void *entity, char **args) {
    check_expected_ptr(entity);
    check_expected_ptr(args);
    if (args)
        check_expected_ptr(args[0]);

    function_called();
    return 42;
}

/**
 * A mock command_handler expecting 2 arguments.
 * @param args the arguments matched in the parsed command
 */
int mock_handler_2_args(void *entity, char **args) {
    check_expected_ptr(entity);
    check_expected_ptr(args);
    check_expected_ptr(args[0]);
    check_expected_ptr(args[1]);

    function_called();
    return 24;
}

/**
 * A helper to instantiate a new parser before each test.
 */
int parser_setup(__attribute__((__unused__)) void **state) {
    assert_ptr_equal(p, NULL);

    struct command commands[] = {
            {"command0",                   0, mock_handler},
            {"command1 (.+)",              1, mock_handler},
            {"command2 ([0-9]+) ([a-z]*)", 2, mock_handler_2_args},

    };
    int nb_commands = sizeof(commands) / sizeof(commands[0]);
    p = parser__init(commands, nb_commands);

    assert_ptr_not_equal(p, NULL);
    return 0;
}

/**
 * TEST: The parser should fail when initialized with an incorrect regex
 */
static void parser__fails_on_wrong_regex(__attribute__((__unused__)) void **state) {
    struct command commands[] = {
            {".*",           0, NULL},
            {"[[:azerty:]]", 0, NULL}, // an incorrect regex
    };
    int nb_commands = sizeof(commands) / sizeof(commands[0]);
    p = parser__init(commands, nb_commands);
    assert_ptr_equal(p, NULL);
}

/**
 * A helper to clean the parser after each test
 */
int parser_teardown(__attribute__((__unused__)) void **state) {
    assert_ptr_not_equal(p, NULL);
    parser__free(p);
    p = NULL;
    return 0;
}

/**
 * TEST: The parser should reject unknown (i.e. non-matching) commands
 */
static void parser__does_not_match_unknown_command(__attribute__((__unused__)) void **state) {
    int res = parser__parse(p, "unknown", NULL);
    assert_int_equal(res, -1);
}

/**
 * TEST: The parser should be able to parser a command with no argument
 * and call the associated handler with NULL
 */
static void parser__matches_with_no_argument(__attribute__((__unused__)) void **state) {
    void *entity = (void *) 0xabc;

    expect_function_call(mock_handler);
    expect_value(mock_handler, entity, entity);
    expect_value(mock_handler, args, NULL);

    int res = parser__parse(p, "command0", entity);
    assert_int_equal(res, 42);
}

/**
 * TEST: The parser should be able to parser a command with one argument
 * and call the associated handler with an array containing this argument
 */
static void parser__matches_with_one_argument(__attribute__((__unused__)) void **state) {
    void *entity = (void *) 0xabc;

    expect_function_call(mock_handler);
    expect_value(mock_handler, entity, entity);
    expect_not_value(mock_handler, args, NULL);
    expect_string(mock_handler, args[0], "abc");

    int res = parser__parse(p, "command1 abc", entity);
    assert_int_equal(res, 42);
}

/**
 * TEST: The parser should reject commands with missing arguments
 */
static void parser__rejects_missing_argument(__attribute__((__unused__)) void **state) {
    int res = parser__parse(p, "command2 123", NULL);
    assert_int_equal(res, -1);
}

/**
 * TEST: The parser should be able to parser a command with two argument
 * and call the associated handler with an array containing these arguments
 */
static void parser__matches_with_two_arguments(__attribute__((__unused__)) void **state) {
    void *entity = (void *) 0xabc;

    expect_function_call(mock_handler_2_args);
    expect_value(mock_handler_2_args, entity, entity);
    expect_not_value(mock_handler_2_args, args, NULL);
    expect_string(mock_handler_2_args, args[0], "123");
    expect_string(mock_handler_2_args, args[1], "abc");

    int res = parser__parse(p, "command2 123 abc", entity);
    assert_int_equal(res, 24);
}


int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(parser__fails_on_wrong_regex),
            cmocka_unit_test_setup_teardown(parser__does_not_match_unknown_command, parser_setup, parser_teardown),
            cmocka_unit_test_setup_teardown(parser__matches_with_no_argument, parser_setup, parser_teardown),
            cmocka_unit_test_setup_teardown(parser__matches_with_one_argument, parser_setup, parser_teardown),
            cmocka_unit_test_setup_teardown(parser__matches_with_two_arguments, parser_setup, parser_teardown),
            cmocka_unit_test_setup_teardown(parser__rejects_missing_argument, parser_setup, parser_teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}