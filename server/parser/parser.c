#include <stdlib.h>
#include <string.h>
#include <regex.h>

#include "../utils/string.h"
#include "../log/log.h"
#include "parser.h"


/**
 * Internal representation of a command
 */
struct compiled_command {
    regex_t regex; /**< a compiled regex associated */
    int arg_count; /**< the number of expected matches */
    command_handler handler; /**< a callback to handle the arguments */
};

/**
 * Internal representation of a parser
 */
struct parser {
    int command_count; /**< the number of commands */
    struct compiled_command *commands; /**< the compiled commands */
};


/**
 * Count the number of groups in a regex match
 * @param matches regex matches from `regexec`
 * @param max_match the number of elements in matches
 * @pre max_match >= 2
 * @note expects a single match containing at least one group
 * @return the number of groups in matches
 */
int count_match_groups(regmatch_t *matches, int max_match) {
    for (int m = 1; m < max_match; m++)
        if (matches[m].rm_so == -1 && matches[m].rm_eo == -1)
            // this match is unused, so it is the last one in the array
            return m - 1;

    return max_match - 1;
}

/**
 * Extract the first `count` group matches from `str`
 * @param str the str being parsed
 * @param count the number of arguments to extract
 * @param matches regex matches from `regexec`
 * @note expects a single match containing at least one group
 * @return an array of strings containing the matches
 */
char **extract_args_from_matches(const char *str, int count, regmatch_t *matches) {
    char **args = calloc(count, sizeof(char *));
    for (int i = 0; i < count; i++) {
        int match_length = matches[i + 1].rm_eo - matches[i + 1].rm_so;
        args[i] = calloc((match_length + 1), sizeof(char));
        strncpy(args[i], &str[matches[i + 1].rm_so], match_length);
        args[i][match_length] = '\0';
    }
    return args;
}

void free_string_array(char **args, int count) {
    for (int i = 0; i < count; i++) {
        free(args[i]);
    }
    free(args);
}

parser parser__init(const command *commands, int count) {
    struct parser *p = calloc(1, sizeof(*p));
    p->commands = calloc(count, sizeof(*p->commands));
    int reg_flags = REG_EXTENDED | REG_NEWLINE | REG_ICASE;

    for (int c = 0; c < count; c++) {
        p->command_count++;
        if (regcomp(&p->commands[c].regex, commands[c].pattern, reg_flags) != 0) {
            log_error("Failed compilation of regex %d: %s", c, commands[c].pattern);
            parser__free(p);
            return NULL;
        }
        p->commands[c].arg_count = commands[c].arg_count;
        p->commands[c].handler = commands[c].handler;
    }

    return p;
}

int parser__parse(parser p, const char *str, void *entity) {
    string escaped_str = escape_string(str);
    log_trace("Parsing \"%s\"", escaped_str->content);

    for (int c = 0; c < p->command_count; c++) {
        struct compiled_command *cmd = &p->commands[c];
        int max_match = cmd->arg_count + 1;
        regmatch_t matches[max_match];

        int matched = regexec(&cmd->regex, str, max_match, matches, 0);
        if (matched == 0) {
            log_trace("Found matching command for \"%s\"", escaped_str->content);
            string__free(escaped_str);

            char **args = NULL;
            if (cmd->arg_count > 0 && count_match_groups(matches, max_match) == cmd->arg_count)
                args = extract_args_from_matches(str, cmd->arg_count, matches);

            int res = p->commands[c].handler(entity, args);

            free_string_array(args, cmd->arg_count);
            return res;
        }
    }

    log_trace("No match found for \"%s\"", escaped_str->content);
    string__free(escaped_str);
    return -1;
}

void parser__free(parser p) {
    log_trace("Freeing a parser");
    for (int c = 0; c < p->command_count; c++) {
        regfree(&p->commands[c].regex);
    }
    free(p->commands);
    free(p);
}
